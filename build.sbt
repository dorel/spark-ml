name := "w2v"

organization := "com.shoeboxed"

version := "0.0.1"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

scalaVersion := "2.10.4"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val akkaV = "2.3.9"
  val sprayV = "1.3.3"
  val sparkV = "1.3.1"
  Seq(
    "io.spray"            %%  "spray-can"     		% sprayV,
    "io.spray"            %%  "spray-routing" 		% sprayV,
    "com.typesafe.akka"   %%  "akka-actor"    		% akkaV,
    "org.apache.spark" 	   %  "spark-core_2.10" 	% sparkV,
    "org.apache.spark" 	   %  "spark-mllib_2.10"  	% sparkV
  )
}

Revolver.settings

//EclipseKeys.withSource := true