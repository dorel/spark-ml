package com.shoeboxed

import scala.util.Random
import scala.util.Try
import org.apache.spark.{ SparkConf, SparkContext }
import org.apache.spark.mllib.classification._
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.feature.Word2Vec
import org.apache.spark.mllib.linalg.{ Vector, Vectors }
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import scala.util.Success
import scala.util.Failure
import java.util.concurrent.ConcurrentHashMap
import akka.actor.Actor

class ClassificationActor extends Actor {
  def receive = {
    case word: String => sender ! Classifier.predict(word)
  }
}

object Classifier {

  val CorpusName = "all-enhanced"
  val CorpusFile = s"/Users/dorel/_spark_models/${CorpusName}"
  val VectorsFile = s"/Users/dorel/_spark_models/${CorpusName}-vectors"
  val TrainedModel = s"/Users/dorel/_spark_models/${CorpusName}-model"

  lazy val CurrentContext = buildContext
  lazy val CurrentModel = loadSavedModel(CurrentContext, TrainedModel)
  lazy val CurrentWord2VecModel: RDD[LabeledPoint] =
    CurrentContext.objectFile(CorpusFile, Runtime.getRuntime.availableProcessors * 2)
  lazy val CurrentVectorsFile: RDD[(String, Vector)] =
    CurrentContext.objectFile(VectorsFile, Runtime.getRuntime.availableProcessors * 2)
  lazy val CurrentVectorsCache = {
    val cache = new ConcurrentHashMap[String, Vector]
    CurrentVectorsFile.collect().foreach(tuple => cache.put(tuple._1, tuple._2))
    println(s" -- Initialized cache size: ${cache.size}")
    cache
  }

  //  def main(args: Array[String]) {
  //    val sc = CurrentContext
  //    //saveWord2VecModel(sc)
  //    //trainAndSaveModel(sc, sc.objectFile(CorpusFile, Runtime.getRuntime.availableProcessors * 2))
  //    println(s" --- Amazon: ${Classifier.predict("Amazon")}")
  //    println(s" --- Diamond: ${Classifier.predict("Diamond")}")
  //    sc.stop()
  //  }

  def buildContext = {
    val conf = new SparkConf()
      .setAppName("Product Name Classifier")
      .setMaster("local[16]")
      .set("spark.driver.maxResultSize", "3g")
      .set("spark.driver.memory", "5g")
      .set("spark.executor.memory", "45g")
    new SparkContext(conf)
  }

  def predict(word: String) = {
    val vector = CurrentVectorsCache.get(word)
    Try {
      CurrentModel.predict(vector)
    } match {
      case Success(res) => res
      case Failure(exc) => { println(s" -- Term '$word' not found in dictionary, returning 0"); 0 }
    }
  }

  def evaluateModel(model: LogisticRegressionModel, test: RDD[LabeledPoint]) = {
    println(" -- Evaluating model...")

    val scoreAndLabels = test.map { point =>
      val score = model.predict(point.features)
      (score, point.label)
    }

    val metrics = new BinaryClassificationMetrics(scoreAndLabels)
    println(s"P/R: ${metrics.areaUnderPR()}")
    println(s"ROC: ${metrics.areaUnderROC()}")
  }

  def buildTrainedModel(sc: SparkContext, word2VecData: RDD[LabeledPoint]) = {
    println(" -- Building the model/ training...")
    val splits = word2VecData.randomSplit(Array(0.85, 0.15), seed = Random.nextLong)
    val training = splits(0)
    val test = splits(1)
    val algo = new LogisticRegressionWithLBFGS()
    (algo.run(training), test)
  }

  def loadSavedModel(sc: SparkContext, path: String) = {
    LogisticRegressionModel.load(sc, path)
  }

  def trainAndSaveModel(sc: SparkContext, data: RDD[LabeledPoint]) = {
    val modelAndTest = buildTrainedModel(sc, data)
    val model = modelAndTest._1
    val test = modelAndTest._2
    model.save(sc, TrainedModel)
    println(" -- Benchmarking the saved model...")
    evaluateModel(model, test)
  }

  def saveWord2VecModel(sc: SparkContext) = {
    //load files
    val fName = s"/Users/dorel/Work/shoeboxed/py-classifier/corpus_ner_big/${CorpusName}/training"
    println(s"Loading $fName")
    val huge = sc.wholeTextFiles(fName, 8)

    // seq[seq[String]] each file being a list (\n\n sep) of lists (sentence formed from ’token POS IOB’ tuples)
    val files = huge.map(file => file._2.split("\n\n").toSeq.map(sent => sent.split("\n").toSeq))

    // extract token sentences only (no more POS/IOB)
    val tokens = files.map(file => file.map(sentence => sentence.map(tuple => tuple.split(" ")(0)).toSeq))

    val sents = tokens.flatMap(f => f)
    val algo = new Word2Vec()
    algo.setMinCount(0)
    println(" -- Training Word2vec...")
    val model = algo.fit(sents)

    Seq("Amazon", "Invoice", "Song", "Product", "Description").foreach { word =>
      Try {
        val results = model.findSynonyms(word, 10).map { case (w, s) => w + ": " + s }.mkString(", ")
        println(s" -- Synonims for '$word': $results")
      }
    }

    println(" -- Training Word2vec done, saving vectors file...")

    val vects = model.getVectors
    val labeledTokens = files.flatMap(a => a)
    val trainingTuples = labeledTokens map tokenToLabel
    val tmpX = trainingTuples.flatMap(a => a)

    val vectorsMap = tmpX.map(tpl => (tpl._1, Vectors.dense(vects(tpl._1).map(_.toDouble))))
    vectorsMap.saveAsObjectFile(VectorsFile)

    println(" -- Vectors file saved, saving corpus file...")

    val X = tmpX.map(tpl => LabeledPoint(tpl._2, Vectors.dense(vects(tpl._1).map(_.toDouble))))

    val posCount = X.filter { _.label == 1d }.count
    val negCount = X.filter { _.label != 1d }.count
    println(s" -- Positives: $posCount Negatives: $negCount")

    X.saveAsObjectFile(CorpusFile)

    println(s"Saved vectors: ${X.count}")
  }

  def tokenToLabel(sent: Seq[String]) = sent.map(s => (s.split(" ")(0), if (s.split(" ")(2) == "O") 0 else 1))

}