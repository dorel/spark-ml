package spray.examples

import akka.actor.{Actor, Props}
import akka.pattern.ask
import scala.concurrent.duration._
import spray.routing._
import akka.util.Timeout
import scala.concurrent.Future
import com.shoeboxed.ClassificationActor

class PredictionServiceActor extends Actor with PredictionService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing,
  // timeout handling or alternative handler registration
  def receive = runRoute(predictionRoute)
}

// this trait defines our service behavior independently from the service actor
trait PredictionService extends HttpService {

  // we use the enclosing ActorContext's or ActorSystem's dispatcher for our Futures and Scheduler
  implicit def executionContext = actorRefFactory.dispatcher
  implicit val askTimeout: Timeout = 5.second
  
  val classifier = actorRefFactory.actorOf(Props[ClassificationActor], "classification-service")

  val predictionRoute = {
    get {
      path("predict") {
        parameters('terms) { terms =>
          //Parallel compute predictions for each term
          val predictions = terms.split(" ").map(x => ask(classifier, x).mapTo[Double]).toSeq
          complete(Future.sequence(predictions).map(_.mkString(" ")))
        }
      }
    }
  }
}